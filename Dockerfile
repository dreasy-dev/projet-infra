FROM php:8.1.9-apache

RUN apt-get update && \
    apt-get install -y \
      libpq-dev \
      libzip-dev \
      libfreetype6-dev \
      libjpeg62-turbo-dev \
      libpng-dev \
      libicu-dev \
      libxslt1-dev \
      libxml2-dev \
      wget \
      unzip && \
    docker-php-ext-configure gd --with-freetype --with-jpeg && \
    docker-php-ext-install \
      pdo \
      pdo_mysql \
      gd \
      intl \
      xsl \
      soap \
      zip && \
     ln -s ../mods-available/rewrite.load /etc/apache2/mods-enabled/

COPY ./ /var/www/html

RUN chmod -R 775 /var/www/html

EXPOSE 80