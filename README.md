# Projet Infrastructure & SI - YSHOP : La boutique de YNOV Campus

## Sommaire

- [I. Présentation du projet](#i-présentation-du-projet)
- [II. Prérequis](#ii-prérequis)
- [III. Installation](#iii-installation)
    - [A - Installation générale](#a-installation-générale)
    - [B - Desactivation du admin approuval](#b-desactivation-du-admin-approuval)
    - [C - Recuperation et installation du token pour le runner](#c-recuperation-et-installation-du-token-pour-le-runner)
- [IV. Utilisation](#iv-utilisation)
- [V. Crédits](#v-crédits)

## I. Présentation du projet

Le site Yshop est destiné à la vente et réservation de goodies YNOV CAMPUS aux étudiants. Il a été réalisé par Maxence GILLES et Luka GARCIA.

L'objectif de notre projet infrastructure & SI est donc de mettre en ligne cette application web, ainsi que de procéder à une automatisation, et une simplification pour la mise en place de notre solution.

Nous avons également développé une API afin de pouvoir rendre les données accessibles, notamment si les étudiants veulent créer des outils à partir de YSHOP (liste des articles, catégories, articles, ...)

## II. Prérequis

Tout d'abord, vous devez disposer d'une machine (n'importe quel OS) avec les outils différents :

- Docker ([Documentation d'installation](https://docs.docker.com/engine/install/))
- Docker-compose ([Documentation d'installation](https://docs.docker.com/compose/install))
- Git ([Documentation d'installation](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git))

Le schema suivant montre l'architecture du projet :

![Architecture](/Rendu/schema.png)

Il faut donc une machine qui va faire tourner le pfSense avant le serveur principal qui va faire tourner les containers.

[Configuration pour le pfSense](/Rendu/config-pfsense.xml)

## III. Installation

### A - Installation générale
Si votre user n'est pas sudoer et/ou dans le groupe de docker, il faut exécuter le script suivant avec les droits d'exécuter sudo

 ``` bash
 #!/bin/bash
    
 ########################################
    usermod -aG docker $USER
    usermod -aG wheel $USER
 ########################################
```

Une fois les prérequis installés, il suffit de cloner le projet et de lancer les containers

```bash
# Clone du projet et lancement des containers
git clone  https://gitlab.com/dreasy-dev/projet-infra.git
cd projet-infra
docker-compose up -d --build 
cd 

# Récupération du mot de passe admin de gitlab WebUi
sleep 60
docker exec $(docker ps -aqf "name=gitlab") cat /etc/gitlab/initial_root_password | grep Password:
docker exec $(docker ps -aqf "name=gitlab") cat /etc/gitlab/initial_root_password | grep Password: > Password.txt
```


### B - Desactivation du admin approuval

Pour desactiver l'approbation de l'admin, il faut se connecter sur la WebUI de gitlab (https://gitlab.yshop.com.co) en user: root et password: `ouvrir le fichier Password.txt` puis aller dans  ``/admin/application_settings/general`` et décocher la case `Require Admin Approuval For new users ` dans `Sign-up restrictions` et enfin cliquer sur `Save changes`.

### C - Recuperation et installation du token pour le runner

Pour récupérer le token du runner, il faut se connecter sur la WebUI de gitlab et aller dans le menu  `CI/CD` et enfin `Runners` et copier le token du runner a la place de `'replace_with_token_runner'`  dans le script ci dessous.

```bash
# Mise en place du token du runner
sudo bash $PWD/token_runner.sh 'replace_with_token_runner'

# Installation du runner sur le serveur gitlab
docker exec  $(docker ps -aqf "name=runner")   gitlab-runner register --non-interactive --url "https://gitlab.yshop.com.co/" --registration-token 'replace_with_token_runner'     --description "codechecker" --executor "docker" --docker-image alpin:latest

# Redémarrage du runner
docker restart $(docker ps -aqf "name=runner")
```

## IV. Utilisation

- Yshop : [https://yshop.com.co](https://yshop.com.co)
- Gitlab : [https://gitlab.yshop.com.co](https://gitlab.yshop.com.co)
- PhpMyAdmin : [https://admin.yshop.com.co](https://admin.yshop.com.co)
- Api : [https://api.yshop.com.co](https://api.yshop.com.co)

## V. Crédits

Ce projet a été réalisé par Maxence GILLES et Luka GARCIA en guise de projet infrastructure & SI de deuxième année à Bordeaux YNOV CAMPUS (2022/2023).