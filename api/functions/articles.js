const db = require('./db');
const config = require('../config');
const helper = require('../helper');

async function getAll(){
  const rows = await db.query(
    `SELECT id, name, description, price, picture, id_filiere, id_user, publication 
    FROM article`
  );
  const data = helper.emptyOrRows(rows);

  return data;
}

async function getOne(id = null) {
  if (!id) {
    return null;
  }
  const rows = await db.query(
    `SELECT id, name, description, price, picture, id_filiere, id_user, publication 
    FROM article WHERE id = ?`,
    [id]
  );
  const data = helper.emptyOrRows(rows);

  return data;
}

module.exports = {
  getAll,
  getOne

}