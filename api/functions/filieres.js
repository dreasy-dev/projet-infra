const db = require('./db');
const config = require('../config');
const helper = require('../helper');

async function getAll(){
  const rows = await db.query(
    `SELECT id, name, color 
    FROM filiere`
  );
  const data = helper.emptyOrRows(rows);

  return data;
}

async function getOne(id = null) {
  if (!id) {
    return null;
  }
  const rows = await db.query(
    `SELECT id, name, color 
    FROM filiere WHERE id = ?`,
    [id]
  );
  const data = helper.emptyOrRows(rows);

  return data;
}

async function create(filiere){
    const result = await db.query(
      `INSERT INTO filiere(name, color) 
      VALUES 
      (?, ?)`,
      [filiere.name, filiere.color]
    );
  
    let message = 'Error in creating programming language';
  
    if (result.affectedRows) {
      message = 'Programming language created successfully';
    }
  
    return {message};
  }

async function remove(id){
    const result = await db.query(
      `DELETE FROM filiere WHERE id = ?`,
      [id]
    );
  
    let message = 'Error in deleting programming language';
  
    if (result.affectedRows) {
      message = 'Programming language deleted successfully';
    }
  
    return {message};
}

module.exports = {
    getAll,
    getOne,
    create,
    remove
  }