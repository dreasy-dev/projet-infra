var express = require('express');
var app = express();
const bp = require('body-parser')

app.use(bp.json())
app.use(bp.urlencoded({ extended: true }))

var articles = require('./functions/articles');
var filieres = require('./functions/filieres');



/* ---------------------- */
/* -----{ Articles }----- */
/* ---------------------- */

var articlesRouter = express.Router();
articlesRouter.get('/', async function(req, res, next) {
    try {
        res.json(await articles.getAll());
    } catch (err) {
        console.error(`Error while getting programming languages `, err.message);
        next(err);
    }
});
articlesRouter.post('/', async function(req, res) {});
articlesRouter.get('/:id', async function(req, res, next) {
    try {
        res.json(await articles.getOne(req.params.id));
    } catch (err) {
        console.error(`Error while getting programming languages `, err.message);
        next(err);
    }
});
articlesRouter.patch('/:id', async function(req, res) {});
articlesRouter.delete('/:id', async function(req, res) {});
app.use('/articles', articlesRouter);



/* ---------------------- */
/* -----{ Filières }----- */
/* ---------------------- */

var filieresRouter = express.Router();
filieresRouter.get('/', async function(req, res, next) {
    try {
        res.json(await filieres.getAll());
    } catch (err) {
        console.error(`Error while getting programming languages `, err.message);
        next(err);
    }
});
filieresRouter.post('/', async function(req, res, next) {
    try {
        console.log(req.body);
        res.json(await filieres.create(req.body));
    } catch (err) {
        console.error(`Error while getting programming languages `, err.message);
        next(err);
    }
});
filieresRouter.get('/:id', async function(req, res, next) {
    try {
        res.json(await filieres.getOne(req.params.id));
    } catch (err) {
        console.error(`Error while getting programming languages `, err.message);
        next(err);
    }
});
filieresRouter.patch('/:id', async function(req, res) {});
filieresRouter.delete('/:id', async function(req, res, next) {
    try {
        res.json(await filieres.remove(req.params.id));
    } catch (err) {
        console.error(`Error while getting programming languages `, err.message);
        next(err);
    }
});
app.use('/filieres', filieresRouter);

app.listen(80, () => { console.log('Serveur prêt') })
module.exports = app;