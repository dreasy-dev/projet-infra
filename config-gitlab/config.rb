### Change the initial default admin password and shared runner registration tokens.
###! **Only applicable on initial setup, changing these settings after database
###!   is created and seeded won't yield any change.**

### Toggle if root password should be printed to STDOUT during initialization
gitlab_rails['display_initial_root_password'] = true
## OmniAuth Settings
##! Docs: https://docs.gitlab.com/ee/integration/omniauth.html
gitlab_rails['omniauth_enabled'] = true
gitlab_rails['omniauth_allow_single_sign_on'] = ['saml']
gitlab_rails['omniauth_sync_email_from_provider'] = 'saml'
gitlab_rails['omniauth_sync_profile_from_provider'] = ['saml']
gitlab_rails['omniauth_sync_profile_attributes'] = ['email']
gitlab_rails['omniauth_auto_sign_in_with_provider'] = 'saml'
gitlab_rails['omniauth_block_auto_created_users'] = false
