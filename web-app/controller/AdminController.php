<?php

require_once('./models/Article.php');
require_once('./models/Filiere.php');
require_once('./models/User.php');

function adminHomeController() {
    // Initialize the session
    if(!isset($_SESSION)){
        session_start();
    }

    if(!userIsAdmin()) {
        require('view/404.php');
        return;
    }
	$page = "home";
	require('view/admin/admin-template.php');
}

function adminUsersController(string $page) {

    if(!userIsAdmin()) {
        require('view/404.php');
        return;
    }

    if($page == "users.delete" || $page == "users.admin") {
        // Processing when delete button is pushed
        if($_SERVER["REQUEST_METHOD"] == "POST"){
        
            if(empty(trim($_POST["id"]))){
                $err = "No id found.";
            } else {
                $id = trim($_POST["id"]);
            }
            
            if(empty($err)){
                switch ($page) {
                    case "users.delete":
                        userDelete($id);
                        break;
                    case "users.admin":
                        userToggleAdmin($id);
                }
                header("location: /admin/users");
            }
        }
    }

    switch ($page) {
        case "users.home":
            $users = getAllUsers();
            break;
        case "users.search":
            $users = searchUsers($_GET['search']);
            break;
        case "users.admins":
            $users = getAdminUsers();
            break;
    }
	require('view/admin/admin-template.php');
}

function adminArticlesController(string $page) {
    if(!userIsAdmin()) {
        require('view/404.php');
        return;
    }

    if($page == "articles.delete" || $page == "articles.add") {
        // Processing when delete button is pushed
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            
            if ($page == "articles.delete") {
                if(empty(trim($_POST["id"]))){
                    $err = "No id found.";
                } else {
                    $id = trim($_POST["id"]);
                }
            } else if ($page == "articles.add") {
                if(empty(trim($_POST["name"])) || empty(trim($_POST["price"])) || empty(trim($_POST["filiere"]))){
                    $err = "No name/price/image/filiere found.";
                } else {
                    $name = trim($_POST["name"]);
                    $price = trim($_POST["price"]);
                    $filiere = trim($_POST["filiere"]);
                }
            }
            
            if(empty($err)){
                // Prepare a delete statement
                switch ($page) {
                    case "articles.delete":
                        $sql = "DELETE FROM article WHERE id = :id";
                        break;
                    case "articles.add":
                        $sql = "INSERT INTO article (name, price, description, id_filiere, id_user, publication) VALUES (:name, :price, :description, :id_filiere, :id_user, :publication)";
                        break;
                }
                
                if($stmt = DB->prepare($sql)){

                    switch ($page) {
                        case "articles.delete":
                            // Bind variables to the prepared statement as parameters
                            $stmt->bindParam(":id", $param_id, PDO::PARAM_STR);
                            // Set parameters
                            $param_id = trim($_POST["id"]);
                            break;
                        case "articles.add":
                            // Bind variables to the prepared statement as parameters
                            $stmt->bindParam(":name", $param_name, PDO::PARAM_STR);
                            $stmt->bindParam(":price", $param_price, PDO::PARAM_STR);
                            $stmt->bindParam(":description", $param_description, PDO::PARAM_STR);
                            $stmt->bindParam(":id_filiere", $param_filiere, PDO::PARAM_STR);
                            $stmt->bindParam(":id_user", $param_id_user, PDO::PARAM_STR);
                            $stmt->bindParam(":publication", $param_publication, PDO::PARAM_STR);
                            // Set parameters
                            $param_name = trim($_POST["name"]);
                            $param_price = trim($_POST["price"]);
                            $param_description = trim($_POST["description"]);
                            $param_filiere = trim($_POST["filiere"]);
                            $param_id_user = $_SESSION['id'];
                            $param_publication = date("Y-m-d H:i:s");
                            break;
                    }              
                    // Attempt to execute the prepared statement
                    if($stmt->execute()){
                        // Redirect
                        switch ($page) {
                            case "articles.delete":
                                header("location: /admin/articles");
                                break;
                            case "articles.add":

                                if ($_FILES['image']['error'] == 0) {
                                    $picture = $_FILES['image'];
                                    $ext = ['png', 'jpeg', 'jpg'];
                                    $filename = $picture['name'];
                                    $filesize = $picture['size'];
                                    $filetmp = $picture['tmp_name'];
                                    $tmp = explode('.', $filename);
                                    $fileext = end($tmp);
                                    if (in_array($fileext, $ext)) {
                                        if ($filesize < 1000000) {
                                            $upload = move_uploaded_file($filetmp, getcwd() . "/view/assets/images/articles/" . DB->lastInsertId() . "." . $fileext);
                                            if ($upload) {
                                                // update db with link
                                                $sql = "UPDATE article SET picture = :picture WHERE id = :id";
                                                if($stmt = DB->prepare($sql)){
                                                    // Bind variables to the prepared statement as parameters
                                                    $stmt->bindParam(":picture", $param_picture, PDO::PARAM_STR);
                                                    $stmt->bindParam(":id", $param_id, PDO::PARAM_STR);
                                                    // Set parameters
                                                    $param_picture = "/view/assets/images/articles/" . DB->lastInsertId() . "." . $fileext;
                                                    $param_id = DB->lastInsertId();
                                                    $stmt->execute();
                                                }
                                            } else {
                                                throw new Exception("Erreur lors de l'upload");
                                            }
                                        } else {
                                            throw new Exception("Image trop lourde");
                                        }
                                    } else {
                                        throw new Exception("Extension non autorisée : " . $fileext);
                                    }                
                                }

                                header("location: /admin/article?id=". DB->lastInsertId());
                                break;
                        }
                        return;
                    } else{
                        echo "Oops! Something went wrong. Please try again later.";
                    }

                    // Close statement
                    unset($stmt);
                }
            }
        }
    }

    switch ($page) {
        case "articles.home":
            $articles = getAllArticles();
            break;
        case "articles.add":
            $filieres = getAllFilieres();
            break;
    }
	require('view/admin/admin-template.php');
}

function adminFilieresController(string $page) {
    if(!userIsAdmin()) {
        require('view/404.php');
        return;
    }

    if($page == "filieres.delete" || $page == "filieres.add") {
        // Processing when delete button is pushed
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            
            if ($page == "filieres.delete") {
                if(empty(trim($_POST["id"]))){
                    $err = "No id found.";
                } else {
                    $id = trim($_POST["id"]);
                }
            } else if ($page == "filieres.add") {
                if(empty(trim($_POST["name"])) || empty(trim($_POST["color"]))){
                    $err = "No name/color found.";
                } else {
                    $name = trim($_POST["name"]);
                    $color = trim($_POST["color"]);
                }
            }
            
            if(empty($err)){
                // Prepare a delete statement
                switch ($page) {
                    case "filieres.delete":
                        filiereDelete($id);
                        break;
                    case "filieres.add":
                        filiereCreate($name, $color);
                        break;
                }
                header("location: /admin/filieres");
            }
        }
    }

    switch ($page) {
        case "filieres.home":
            $filieres = getAllFilieres();
            break;
    }
    require('view/admin/admin-template.php');
}