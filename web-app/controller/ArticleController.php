<?php

require_once('./models/Article.php');
require_once('./models/Cart.php');

function articleController($id) {
	if($id == null || $id == 0 || !is_numeric($id) || getArticle($id) == null) {
		require('view/404.php');
		return;
	}
	$article = getArticle($id);
	require('view/article.php');
}

function articlesController($filiere = null) {
	if ($filiere == null) {
		$articles = getAllArticles();
	} else {
		$articles = getArticlesByFiliere($filiere);
	}
	require('view/articles.php');
}