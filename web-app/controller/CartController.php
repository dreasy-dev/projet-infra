<?php

require_once('./models/Article.php');
require_once('./models/Cart.php');
require_once('./models/Stock.php');

function cartController() {
    if (userIsConnected()) {
        $cart = getCart();
        require('view/cart.php');
    } else {
        header('Location: /login');
    }
}

function addToCartController() {
	if (userIsConnected()) {
		if (isset($_POST['id']) && isset($_POST['stock'])) {
			$id_user = userGetId();
			$id_article = $_POST['id'];
			$id_stock = $_POST['stock'];
			addToCart($id_user, $id_article, $id_stock);
			header('Location: /article?id=' . $id_article);
		} else {
			header('Location: /');
		}
	} else {
		header('Location: /login');
	}
}

function removeFromCartController() {
	if (userIsConnected()) {
		if (isset($_POST['id'])) {
			$id = $_POST['id'];
			removeFromCart($id);
			header('Location: /cart');
		} else {
			header('Location: /');
		}
	} else {
		header('Location: /login');
	}
}

function commandController() {
	if (userIsConnected()) {
		if (isset($_POST)) {

			// Calculate the total amount of the invoice
			$cart = getCart(); // [] -> [id, id_article, id_stock]
			$amount = 0;
			foreach ($cart as $item) {
				$article = getArticle($item['id_article']);
				$amount += $article['price'];
			}
			
			// Create an invoice line in the db
			$id_user = userGetId();
			$req = DB->prepare('INSERT INTO invoice (id_user, date, amount, status) VALUES (:id_user, NOW(),' . $amount . ', "En attente")');
			$req->bindParam(':id_user', $id_user, PDO::PARAM_STR);
			$req->execute();

			// Create a line for each item in the cart
			$id_invoice = DB->lastInsertId();
			foreach ($cart as $item) {
				$req = DB->prepare('INSERT INTO invoice_article (id_invoice, id_article, id_stock) VALUES (:id_invoice, :id_article, :id_stock)');
				$req->bindParam(':id_invoice', $id_invoice, PDO::PARAM_STR);
				$req->bindParam(':id_article', $item['id_article'], PDO::PARAM_STR);
				$req->bindParam(':id_stock', $item['id_stock'], PDO::PARAM_STR);
				$req->execute();
			}

			// Delete the cart
			emptyCart();

			// Remove the Ycoins
			userSetCoins($_SESSION['coins'] - $amount);

			require('view/command.php');
		} else {
			header('Location: /');
		}
	} else {
		header('Location: /login');
	}
}