<?php

require_once('./models/User.php');

function loginController() {
    // Initialize the session
    if(!isset($_SESSION)){
        session_start();
    }

    // Check if the user is already logged in, if yes then redirect him to welcome page
    if(userIsConnected()){
        header("location: /");
        exit;
    }
    
    
    // Define variables and initialize with empty values
    $email = $password = "";
    $email_err = $password_err = $login_err = "";
    
    // Processing form data when form is submitted
    if($_SERVER["REQUEST_METHOD"] == "POST"){
    
        // Check if username is empty
        if(empty(trim($_POST["email"]))){
            $email_err = "Please enter e-mail.";
        } else{
            $email = trim($_POST["email"]);
        }
        
        // Check if password is empty
        if(empty(trim($_POST["password"]))){
            $password_err = "Please enter your password.";
        } else{
            $password = trim($_POST["password"]);
        }
        
        // Validate credentials
        if(empty($email_err) && empty($password_err)){
            // Prepare a select statement
            $sql = "SELECT id, firstName, lastName, password, credits, salt, picture, role FROM user WHERE mail = :email";
            
            if($stmt = DB->prepare($sql)){
                // Bind variables to the prepared statement as parameters
                $stmt->bindParam(":email", $param_email, PDO::PARAM_STR);
                
                // Set parameters
                $param_email = trim($_POST["email"]);
                
                // Attempt to execute the prepared statement
                if($stmt->execute()){
                    // Check if username exists, if yes then verify password
                    if($stmt->rowCount() == 1){
                        if($row = $stmt->fetch()){
                            $id = $row["id"];
                            $firstName = $row["firstName"];
                            $lastName = $row["lastName"];
                            $picture = $row["picture"];
                            if ($row["role"] != null) {
                                $role = $row["role"];
                            } else {
                                $role = "user";
                            }
                            $salt = $row["salt"];
                            $hashed_password = $row["password"];
                            if(checkPassword($password, $salt, $hashed_password)){                                
                                // Store data in session variables
                                $_SESSION["loggedin"] = true;
                                $_SESSION["id"] = $id;
                                $_SESSION["firstName"] = $firstName;
                                $_SESSION["lastName"] = $lastName;
                                $_SESSION["email"] = $email;   
                                $_SESSION["picture"] = $picture;
                                $_SESSION["role"] = $role;
                                $_SESSION["coins"] = $row["credits"];
                                
                                // Redirect user to welcome page
                                header("location: /");
                            } else{
                                // Password is not valid, display a generic error message
                                $login_err = "Invalid email or password.";
                            }
                        }
                    } else{
                        // Username doesn't exist, display a generic error message
                        $login_err = "Invalid email or password.";
                    }
                } else{
                    echo "Oops! Something went wrong. Please try again later.";
                }

                // Close statement
                unset($stmt);
            }
        }
        
        // Close connection
        unset($pdo);
    }
    require('view/login.php');
}

function checkPassword($password, $salt, $hashed_password) {
    return ($hashed_password === hash("sha512", $password . $salt));
}