<?php

function logoutController() {

    if(!userIsConnected()) {
        header("location: /");
        exit;
    }

    // Initialize the session
    if(!isset($_SESSION)){
        session_start();
    }
    
    // Unset all of the session variables
    $_SESSION = array();
    
    // Destroy the session.
    session_destroy();
    unset($_SESSION);
    
    // Redirect to login page
    header("location: /");
    exit;
}