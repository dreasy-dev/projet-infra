<?php

require_once('./models/User.php');

function profileController($page) {
	if(!isset($_SESSION)){
        session_start();  
    }

	if ($page == "commands") {
		// Get all commands
		$commands = userGetCommands();

		// Get all commands in progress
		$commandsInProgress = userGetCommands("En attente");

		// Get all commands delivered
		$commandsDelivered = userGetCommands("Terminée");

		// Get all commands canceled
		$commandsCanceled = userGetCommands("Annulée");
	}

	if ($page == "infos.modify") {
		// Processing when button is pushed
        if($_SERVER["REQUEST_METHOD"] == "POST") {
        
            if(empty(trim($_POST["firstName"])) || empty(trim($_POST["lastName"])) || empty(trim($_POST["email"]))) {
                $err = "No firstName/lastName/email found.";
            } else {
                $firstName = trim($_POST["firstName"]);
				$lastName = trim($_POST["lastName"]);
				$email = trim($_POST["email"]);

				// Check if email is valid
				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$err = "Invalid email format";
				}

				// Check if email is already used
				$sql = "SELECT id FROM user WHERE mail = :email";
				if($stmt = DB->prepare($sql)){
					// Bind variables to the prepared statement as parameters
					$stmt->bindParam(":email", $param_email, PDO::PARAM_STR);

					// Set parameters
					$param_email = $email;

					// Attempt to execute the prepared statement
					if($stmt->execute()){
						if($stmt->rowCount() == 1){
							$err = "This email is already used.";
						}
					} else{
						echo "Oops! Something went wrong. Please try again later.";
					}

					// Close statement
                    unset($stmt);
				}

				// Validate name
				if(empty(trim($_POST["firstName"]))){
					$firstName_err = "Please enter a first name.";
				} elseif(!preg_match('/^[a-zA-Z0-9_]+$/', trim($_POST["firstName"]))){
					$firstName_err = "First name can only contain letters, numbers, and underscores.";
				} else {
					$firstName = trim($_POST["firstName"]);
				}
				if (empty(trim($_POST["lastName"]))){
					$lastName_err = "Please enter a last name.";
				} elseif(!preg_match('/^[a-zA-Z0-9_]+$/', trim($_POST["lastName"]))){
					$lastName_err = "Last name can only contain letters, numbers, and underscores.";
				} else {
					$lastName = trim($_POST["lastName"]);
				}
				
				// Validate password
				if(empty(trim($_POST["password"]))){
					$password = "";     
				} elseif(strlen(trim($_POST["password"])) < 6){
					$password_err = "Password must have atleast 6 characters.";
				} else{
					$password = trim($_POST["password"]);
					// Validate confirm password
					if(empty(trim($_POST["passwordConfirm"]))){
						$confirm_password_err = "Please confirm password.";     
					} else{
						$confirm_password = trim($_POST["passwordConfirm"]);
						if(empty($password_err) && ($password != $confirm_password)){
							$confirm_password_err = "Password did not match.";
						}
					}
				}
            }
            
            if(empty($err) || empty($firstName_err) || empty($lastName_err) || empty($password_err) || empty($confirm_password_err)){
				if (!empty($password)) {
					// get user salt
					$sql = "SELECT salt FROM user WHERE id = :id";
					if($stmt = DB->prepare($sql)){
						// Bind variables to the prepared statement as parameters
						$stmt->bindParam(":id", $param_id, PDO::PARAM_STR);

						// Set parameters
						$param_id = $_SESSION["id"];

						// Attempt to execute the prepared statement
						if($stmt->execute()){
							if($stmt->rowCount() == 1){
								$row = $stmt->fetch();
								$salt = $row["salt"];
							}
						} else{
							echo "Oops! Something went wrong. Please try again later.";
						}

						// Close statement
						unset($stmt);
					}
					$hashed_passwd = hash("sha512", $password . $salt);

					$sql = "UPDATE user SET firstName = :firstName, lastName = :lastName, password = :password, mail = :mail WHERE id = :id";
				} else {
					$sql = "UPDATE user SET firstName = :firstName, lastName = :lastName, mail = :mail WHERE id = :id";
				}
                
                if($stmt = DB->prepare($sql)){
                    // Bind variables to the prepared statement as parameters
                    $stmt->bindParam(":id", $param_id, PDO::PARAM_STR);
					$stmt->bindParam(":firstName", $param_firstName, PDO::PARAM_STR);
					$stmt->bindParam(":lastName", $param_lastName, PDO::PARAM_STR);
					if (!empty($password)) {
						$stmt->bindParam(":password", $param_password, PDO::PARAM_STR);
					}
					$stmt->bindParam(":mail", $param_mail, PDO::PARAM_STR);
                    
                    // Set parameters
                    $param_id = $_SESSION["id"];
					$param_firstName = $firstName;
					$param_lastName = $lastName;
					if (!empty($password)) {
						$param_password = $hashed_passwd;
					}
					$param_mail = $email;
                    
                    // Attempt to execute the prepared statement
                    if($stmt->execute()){
						// Change data in current session
						$_SESSION["firstName"] = $firstName;
						$_SESSION["lastName"] = $lastName;
						$_SESSION["email"] = $email;

                        // Redirect to users page
                        header("location: /profile/infos");
                        return;
                    } else{
                        echo "Oops! Something went wrong. Please try again later.";
                    }

                    // Close statement
                    unset($stmt);
                }
            }
            
            // Close connection
            unset($pdo);
        }
	}
	require('view/profile/profile-template.php');
	return $page;
}