<?php

require_once('./models/User.php');

function registerController() {
    if (userIsConnected()) {
        header("location: /");
        exit;
    }

    // Define variables and initialize with empty values
    $firstName_err = $lastName_err = $email_err = $password_err = $confirm_password_err = "";

    if($_SERVER["REQUEST_METHOD"] == "POST"){

        // Validate email
        if(empty(trim($_POST["email"]))){
            $email_err = "Please enter a valid email.";
        } elseif (!filter_var(trim($_POST["email"]), FILTER_VALIDATE_EMAIL)) {
            $email_err = "Invalid email format";
        } else {
            // Prepare a select statement
            $sql = "SELECT id FROM user WHERE mail = :email";
            
            if($stmt = DB->prepare($sql)){

                // Bind variables to the prepared statement as parameters
                $stmt->bindParam(":email", $param_email, PDO::PARAM_STR);
                    
                // Set parameters
                $param_email = trim($_POST["email"]);
                
                // Attempt to execute the prepared statement
                if($stmt->execute()){
                    if($stmt->rowCount() == 1){
                        $email_err = "This e-mail is already used.";
                    } else{
                        $email = trim($_POST["email"]);
                    }
                } else{
                    echo "Oops! Something went wrong. Please try again later.";
                }

                // Close statement
                unset($stmt);
            }
        }
 
        // Validate name
        if(empty(trim($_POST["firstName"]))){
            $firstName_err = "Please enter a first name.";
        } elseif(!preg_match('/^[a-zA-Z0-9_]+$/', trim($_POST["firstName"]))){
            $firstName_err = "First name can only contain letters, numbers, and underscores.";
        } else {
            $firstName = trim($_POST["firstName"]);
        }
        if (empty(trim($_POST["lastName"]))){
            $lastName_err = "Please enter a last name.";
        } elseif(!preg_match('/^[a-zA-Z0-9_]+$/', trim($_POST["lastName"]))){
            $lastName_err = "Last name can only contain letters, numbers, and underscores.";
        } else {
            $lastName = trim($_POST["lastName"]);
        }
        
        // Validate password
        if(empty(trim($_POST["password"]))){
            $password_err = "Please enter a password.";     
        } elseif(strlen(trim($_POST["password"])) < 6){
            $password_err = "Password must have atleast 6 characters.";
        } else{
            $password = trim($_POST["password"]);
        }
        
        // Validate confirm password
        if(empty(trim($_POST["confirm_password"]))){
            $confirm_password_err = "Please confirm password.";     
        } else{
            $confirm_password = trim($_POST["confirm_password"]);
            if(empty($password_err) && ($password != $confirm_password)){
                $confirm_password_err = "Password did not match.";
            }
        }
        
        // Check input errors before inserting in database
        if(empty($firstName_err) && empty($lastName_err) && empty($password_err) && empty($confirm_password_err)){
            // Generate a salt
            $salt = random_str_generator(8);

            // Prepare an insert statement
            $sql = "INSERT INTO user (firstName, lastName, mail, password, salt, credits) VALUES (:firstName, :lastName, :email, :password, :salt, 0)";
             
            if($stmt = DB->prepare($sql)){
                // Bind variables to the prepared statement as parameters
                $stmt->bindParam(":firstName", $param_firstName, PDO::PARAM_STR);
                $stmt->bindParam(":lastName", $param_lastName, PDO::PARAM_STR);
                $stmt->bindParam(":email", $param_email, PDO::PARAM_STR);
                $stmt->bindParam(":password", $param_password, PDO::PARAM_STR);
                $stmt->bindParam(":salt", $param_salt, PDO::PARAM_STR);
                
                // Set parameters
                $param_firstName = $firstName;
                $param_lastName = $lastName;
                $param_email = $email;
                $param_password = hash("sha512", $password . $salt); // Creates a password hash
                $param_salt = $salt;
                
                // Attempt to execute the prepared statement
                if($stmt->execute()){
                    // Automatically connect user
                    $_SESSION["loggedin"] = true;
                    $_SESSION["id"] = DB->lastInsertId();
                    $_SESSION["firstName"] = $firstName;
                    $_SESSION["lastName"] = $lastName;
                    $_SESSION["email"] = $email;   
                    $_SESSION["picture"] = null; // Default picture will be applied
                    $_SESSION["role"] = null; // Default role will be applied
                    $_SESSION["coins"] = 0;

                    // Redirect user to welcome page
                    header("location: /");
                } else{
                    echo "Oops! Something went wrong. Please try again later.";
                }
    
                // Close statement
                unset($stmt);
            }
        }
        
        // Close connection
        unset($pdo);
    }
    require('view/register.php');
}


function random_str_generator ($len_of_gen_str){
    $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@*!$&#-+./;";
    $var_size = strlen($chars); 
    $random_str = "";
    for( $x = 0; $x < $len_of_gen_str; $x++ ) {  
        $random_str .= $chars[ rand( 0, $var_size - 1 ) ]; 
    }
    return $random_str;
}