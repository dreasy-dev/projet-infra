<?php

// CONFIGURATION
require_once './config/config.php';

// ROUTING SYSTEM
require_once './controller/router/Request.php';
require_once './controller/router/Router.php';
$router = new Router(new Request);

// CONTROLLERS
require_once './controller/HomeController.php';
require_once './controller/ArticleController.php';
require_once './controller/CartController.php';
require_once './controller/user/ProfileController.php';
require_once './controller/user/LoginController.php';
require_once './controller/user/RegisterController.php';
require_once './controller/user/LogoutController.php';
require_once './controller/AdminController.php';

// Initialize the session
if(!isset($_SESSION)){
  session_start();
}


// ROUTE - HOME
$router->get('/', function() {
  homeController();
});

// ROUTE  - ARTICLES
$router->get('/article', function($request) {
  if(array_key_exists('id', $request->getBody())) {
    articleController($request->getBody()["id"]);
  } else {
    require('view/404.php');
  }
});

$router->get('/articles', function($request) {
  if(array_key_exists('filiere', $request->getBody())) {
    articlesController($request->getBody()["filiere"]);
  } else {
    articlesController();
  }
});

// ROUTE - CART & BUYING ZONE

$router->get('/cart', function() {
  cartController();
});

$router->post('/cart/add', function() {
  addToCartController();
});

$router->post('/cart/remove', function() {
  removeFromCartController();
});

$router->post('/cart/buy', function() {
  commandController();
});

// ROUTE - FILIERES
$router->get('/filiere', function($request) {
  if(array_key_exists('id', $request->getBody())) {
    filiereController($request->getBody()["id"]);
  } else {
    require('view/404.php');
  }
});

// ROUTE - USERS
$router->get('/register', function() {
  registerController();
});

$router->post('/register', function() {
  registerController();
});

$router->get('/login', function() {
  loginController();
});

$router->post('/login', function() {
  loginController();
});

$router->get('/logout', function() {
  logoutController();
});

// PRFOILE PAGE

$router->get('/profile', function() {
  profileController("home");
});

$router->get('/profile/infos', function() {
  profileController("infos");
});

$router->get('/profile/infos/modify', function() {
  profileController("infos.modify");
});

$router->post('/profile/infos/modify', function() {
  profileController("infos.modify");
});

$router->get('/profile/commands', function() {
  profileController("commands");
});

$router->get('/profile/ycoins', function() {
  profileController("ycoins");
});

$router->get('/profile/ycoins/send', function() {
  profileController("ycoins-send");
});

// ADMIN ZONE

$router->get('/admin', function() {
  adminHomeController();
});



$router->get('/admin/users', function() {
  adminUsersController("users.home");
});
$router->get('/admin/users/search', function() {
  adminUsersController("users.search");
});
$router->get('/admin/users/admins', function() {
  adminUsersController("users.admins");
});
$router->get('/admin/user', function($request) {
  if(array_key_exists('id', $request->getBody())) {
    adminUsersController("users.one", $request->getBody()["id"]);
  } else {
    require('view/404.php');
  }
});
$router->post('/admin/users/delete', function() {
  adminUsersController("users.delete");
});
$router->post('/admin/users/admin', function() {
  adminUsersController("users.admin");
});



$router->get('/admin/articles', function() {
  adminArticlesController("articles.home");
});
$router->get('/admin/articles/add', function() {
  adminArticlesController("articles.add");
});
$router->post('/admin/articles/add', function() {
  adminArticlesController("articles.add");
});
$router->post('/admin/articles/delete', function() {
  adminArticlesController("articles.delete");
});
$router->get('/admin/article', function($request) {
  if(array_key_exists('id', $request->getBody())) {
    adminUsersController("articles.one", $request->getBody()["id"]);
  } else {
    require('view/404.php');
  }
});



$router->get('/admin/filieres', function() {
  adminFilieresController("filieres.home");
});
$router->get('/admin/filieres/add', function() {
  adminFilieresController("filieres.add");
});
$router->post('/admin/filieres/add', function() {
  adminFilieresController("filieres.add");
});
$router->post('/admin/filieres/delete', function() {
  adminFilieresController("filieres.delete");
});
$router->get('/admin/filiere', function($request) {
  if(array_key_exists('id', $request->getBody())) {
    adminFilieresController("filieres.one", $request->getBody()["id"]);
  } else {
    require('view/404.php');
  }
});


$router->get('/admin/stats', function() {
  adminStatsController();
});