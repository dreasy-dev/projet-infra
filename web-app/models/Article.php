<?php

function getArticle(int $id) : array|null {
    $req = DB->query('SELECT * FROM article WHERE id = ' . $id);
    $row = $req->fetch();
    if ($row === false) {
        return null;
    }

    $reqstock = DB->query('SELECT id, size, number FROM stock WHERE id_article = ' . $id);
    $stocks = [];
    while ($rowstock = $reqstock->fetch()) {
        $stocks[] = [
            'id' => $rowstock['id'],
            'size' => $rowstock['size'],
            'number' => $rowstock['number'],
        ];
    }

    $article = [
        'name' => $row['name'],
        'price' => $row['price'],
        'picture' => $row['picture'],
        'publication' => $row['publication'],
        'description' => $row['description'],
        'stocks' => $stocks,
    ];

    return $article;
}

function getHomeArticles() : array {
    $req = DB->query('SELECT id, name, price, picture, publication, description FROM article WHERE id_filiere = 0 ORDER BY id ASC LIMIT 3');
    
    $articlesWithoutStocks = [];
    while ($row = $req->fetch()) {
        $articlesWithoutStocks[] = [
            'id' => $row['id'],
            'name' => $row['name'],
            'price' => $row['price'],
            'picture' => $row['picture'],
            'publication' => $row['publication'],
            'description' => $row['description'],
        ];
    }

    $articles = [];
    foreach ($articlesWithoutStocks as $article) {
        $reqstock = DB->query('SELECT id, size, number FROM stock WHERE id_article = ' . $article['id']);
        $stocks = [];
        while ($rowstock = $reqstock->fetch()) {
            $stocks[] = [
                'id' => $rowstock['id'],
                'size' => $rowstock['size'],
                'number' => $rowstock['number'],
            ];
        }
        $articles[] = [
            'id' => $article['id'],
            'name' => $article['name'],
            'price' => $article['price'],
            'picture' => $article['picture'],
            'publication' => $article['publication'],
            'description' => $article['description'],
            'stocks' => $stocks,
        ];
    }


    return $articles;
}

function getAllArticles() : array {
    $req = DB->query('SELECT id, name, price, picture, DATE_FORMAT(publication, "%d/%m/%Y") AS publication, description, (SELECT name FROM filiere WHERE article.id_filiere = filiere.id ) AS filiere FROM article');
    
    $articlesWithoutStocks = [];
    while ($row = $req->fetch()) {
        $articlesWithoutStocks[] = [
            'id' => $row['id'],
            'name' => $row['name'],
            'price' => $row['price'],
            'picture' => $row['picture'],
            'publication' => $row['publication'],
            'description' => $row['description'],
            'filiere' => $row['filiere'],
        ];
    }

    $articles = [];
    foreach ($articlesWithoutStocks as $article) {
        $reqstock = DB->query('SELECT id, size, number FROM stock WHERE id_article = ' . $article['id']);
        $stocks = [];
        while ($rowstock = $reqstock->fetch()) {
            $stocks[] = [
                'id' => $rowstock['id'],
                'size' => $rowstock['size'],
                'number' => $rowstock['number'],
            ];
        }
        $articles[] = [
            'id' => $article['id'],
            'name' => $article['name'],
            'price' => $article['price'],
            'picture' => $article['picture'],
            'publication' => $article['publication'],
            'description' => $article['description'],
            'filiere' => $article['filiere'],
            'stocks' => $stocks,
        ];
    }


    return $articles;
}

function getArticlesByFiliere($filiere_id) : array {
    $req = DB->query('SELECT id, name, price, picture, publication, description, (SELECT name FROM filiere WHERE article.id_filiere = filiere.id ) AS filiere FROM article WHERE id_filiere = ' . $filiere_id);
    
    $articlesWithoutStocks = [];
    while ($row = $req->fetch()) {
        $articlesWithoutStocks[] = [
            'id' => $row['id'],
            'name' => $row['name'],
            'price' => $row['price'],
            'picture' => $row['picture'],
            'publication' => $row['publication'],
            'description' => $row['description'],
            'filiere' => $row['filiere'],
        ];
    }

    $articles = [];
    foreach ($articlesWithoutStocks as $article) {
        $reqstock = DB->query('SELECT size, number FROM stock WHERE id_article = ' . $article['id']);
        $stocks = [];
        while ($rowstock = $reqstock->fetch()) {
            $stocks[] = [
                'size' => $rowstock['size'],
                'number' => $rowstock['number'],
            ];
        }
        $articles[] = [
            'id' => $article['id'],
            'name' => $article['name'],
            'price' => $article['price'],
            'picture' => $article['picture'],
            'publication' => $article['publication'],
            'description' => $article['description'],
            'filiere' => $article['filiere'],
            'stocks' => $stocks,
        ];
    }
    return $articles;
}