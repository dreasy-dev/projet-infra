<?php

function getCart() {
    if (userIsConnected()) {
        $cart = [];
        $id_user = userGetId();
        $req = DB->query('SELECT * FROM cart WHERE id_user = ' . $id_user);
        while ($row = $req->fetch()) {
            $cart[] = [
                'id' => $row['id'],
                'id_article' => $row['id_article'],
                'id_stock' => $row['id_stock'],
            ];
        }
        return $cart;
    }
}

function addToCart($id_user, $id_article, $id_stock) {
    $req = DB->prepare('INSERT INTO cart (id_user, id_article, id_stock) VALUES (:id_user, :id_article, :id_stock)');
    $req->bindParam(':id_user', $id_user, PDO::PARAM_STR);
    $req->bindParam(':id_article', $id_article, PDO::PARAM_STR);
    $req->bindParam(':id_stock', $id_stock, PDO::PARAM_STR);
    $req->execute();
}

function removeFromCart($id) {
    $req = DB->prepare('DELETE FROM cart WHERE id = :id');
    $req->bindParam(':id', $id, PDO::PARAM_STR);
    $req->execute();
}

function emptyCart() {
    if (userIsConnected()) {
        $id_user = userGetId();
        $req = DB->prepare('DELETE FROM cart WHERE id_user = :id_user');
        $req->bindParam(':id_user', $id_user, PDO::PARAM_STR);
        $req->execute();
    }
}