<?php

function getAllFilieres() : array {
    $req = DB->query('SELECT * FROM filiere');
    $filieres = [];
    while ($row = $req->fetch()) {
        $filieres[] = [
            'id' => $row['id'],
            'name' => $row['name'],
            'color' => $row['color'],
        ];
    }
    return $filieres;
}

function getFiliereName($id) : string {
    if ($id == 0) {
        return 'Hors filières';
    }
    $req = DB->query('SELECT name FROM filiere WHERE id = ' . $id);
    $row = $req->fetch();
    return $row['name'];
}

function getFiliereColor($id) : string {
    $req = DB->query('SELECT color FROM filiere WHERE id = ' . $id);
    $row = $req->fetch();
    return $row['color'];
}

function filiereDelete($id = null) {
    if ($id !== null) {
        $req = DB->prepare('DELETE FROM filiere WHERE id = :id');
        $req->bindParam(":id", $param_id, PDO::PARAM_STR);
        $param_id = $id;
        $req->execute();
        unset($req);
    }
}

function filiereCreate($name = "Undefined", $color = "#000000") {
    $req = DB->prepare('INSERT INTO filiere (name, color) VALUES (:name, :color)');
    $req->bindParam(':name', $name, PDO::PARAM_STR);
    $req->bindParam(':color', $color, PDO::PARAM_STR);
    $req->execute();
    unset($req);
}