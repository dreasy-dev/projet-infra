<?php

function invoiceGetArticles($id) {
    $db = dbConnect();
    $req = $db->prepare('SELECT * FROM invoice_articles WHERE id_invoice = ?');
    $req->execute(array($id));
    $articles = $req->fetchAll();
    $req->closeCursor();
    return $articles;
}