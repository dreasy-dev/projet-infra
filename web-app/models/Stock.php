<?php

function getStock($id) {
    $req = DB->query('SELECT * FROM stock WHERE id = ' . $id);
    $row = $req->fetch();
    return [
        'id' => $row['id'],
        'id_article' => $row['id_article'],
        'size' => $row['size'],
        'number' => $row['number'],
    ];
}