<?php

function getAllUsers() : array {
    $req = DB->query('SELECT id, firstName, lastName, mail, credits, role FROM user');
    
    $users = [];
    while ($row = $req->fetch()) {
        $users[] = [
            'id' => $row['id'],
            'firstName' => $row['firstName'],
            'lastName' => $row['lastName'],
            'mail' => $row['mail'],
            'credits' => $row['credits'],
            'role' => $row['role'],
        ];
    }

    return $users;
}

function getAdminUsers() : array {
    $req = DB->query('SELECT id, firstName, lastName, mail, credits, role FROM user WHERE role = "admin"');
    
    $users = [];
    while ($row = $req->fetch()) {
        $users[] = [
            'id' => $row['id'],
            'firstName' => $row['firstName'],
            'lastName' => $row['lastName'],
            'mail' => $row['mail'],
            'credits' => $row['credits'],
            'role' => $row['role'],
        ];
    }

    return $users;
}

function userDelete($id = null) {
    if ($id !== null) {
        $req = DB->prepare('DELETE FROM user WHERE id = :id');
        if($stmt = DB->prepare($req)){
            $stmt->bindParam(":id", $param_id, PDO::PARAM_STR);
            $param_id = $id;
            $req->execute();
        }
        unset($req);
        unset($stmt);
    }
}

function userToggleAdmin($id = null) {
    if ($id !== null) {
        if (userIsAdmin($id)) {
            $req = "UPDATE user SET role = null WHERE id = :id";
        } else {
            $req = "UPDATE user SET role = 'admin' WHERE id = :id";
        }
        if($stmt = DB->prepare($req)){
            $stmt->bindParam(":id", $param_id, PDO::PARAM_STR);
            $param_id = $id;
            $stmt->execute();
        }
        unset($req);
        unset($stmt);
    }
}

function userIsConnected() {
    if(!isset($_SESSION)){
        session_start();  
    }  
    return (array_key_exists("loggedin", $_SESSION) && $_SESSION["loggedin"] === true);
}

function userIsAdmin($id = null) {
    if ($id !== null) {
        $req = DB->query('SELECT role FROM user WHERE id = ' . $id);
        $row = $req->fetch();
        return ($row['role'] === "admin");
    }
    if(!isset($_SESSION)){
        session_start();  
    }
    return (array_key_exists("role", $_SESSION) &&  $_SESSION["role"] === "admin");
}

function userGetId() {
    if(!isset($_SESSION)){
        session_start();  
    }
    if (array_key_exists("id", $_SESSION) && $_SESSION["id"] !== null) {
        return ($_SESSION["id"]);
    }
    return 0;
}

function userGetName() {
    return (userGetFirstName() . " " . userGetLastName());
}

function userGetFirstName() {
    if(!isset($_SESSION)){
        session_start();  
    }
    if (array_key_exists("firstName", $_SESSION) && $_SESSION["firstName"] !== null) {
        return ($_SESSION["firstName"]);
    }
    return "Undefined";
}

function userGetLastName() {
    if(!isset($_SESSION)){
        session_start();  
    }
    if (array_key_exists("lastName", $_SESSION) && $_SESSION["lastName"] !== null) {
        return ($_SESSION["lastName"]);
    }
    return "Undefined";
}

function userGetEmail() {
    if(!isset($_SESSION)){
        session_start();  
    }
    if (array_key_exists("email", $_SESSION) && $_SESSION["email"] !== null) {
        return ($_SESSION["email"]);
    }
    return "Undefined";
}

function userGetPicture() {
    if(!isset($_SESSION)){
        session_start();
    }
    if(userIsConnected()) {
        if (array_key_exists("picture", $_SESSION) && $_SESSION["picture"] !== null && !empty($_SESSION["picture"])) {
            return ($_SESSION["picture"]);
        }
        return "/view/assets/images/users/default.png";
    }
    return "/view/assets/images/users/disconnected.png";
}

function userGetCoins() {
    if(!isset($_SESSION)){
        session_start();  
    }
    if (array_key_exists("coins", $_SESSION) && $_SESSION["coins"] !== null) {
        return ($_SESSION["coins"]);
    }
    return 0;
}

function userSetCoins(int $coins = 0) {
    if(!isset($_SESSION)){
        session_start();  
    }
    if ($coins < 0) {
        return;
    }
    try {
        $req = DB->prepare('UPDATE user SET credits = :coins WHERE id = :id_user');
        // $req->bindParam(':id_user', $id_user, PDO::PARAM_STR);
        $data = [
            'id_user' => userGetId(),
            'coins' => $coins,
        ];
        $req->execute($data);
        $_SESSION["coins"] = $coins;
    } catch (PDOException $e) {
        var_dump($e->getMessage());
        die();
    }
}

function userGetCommands($status = "all", $id = null) {
    if ($id !== null) {
        if ($status === "all") {
            $req = DB->query('SELECT id, date, amount, status FROM invoice WHERE id_user = ' . $id);
        } else {
            $req = DB->query('SELECT id, date, amount, status FROM invoice WHERE id_user = ' . $id . ' AND status = "' . $status . '"');
        }
    } else {
        if ($status === "all") {
            $req = DB->query('SELECT id, date, amount, status FROM invoice WHERE id_user = ' . userGetId());
        } else {
            $req = DB->query('SELECT id, date, amount, status FROM invoice WHERE id_user = ' . userGetId() . ' AND status = "' . $status . '"');
        }
    }
    
    $commands = [];
    while ($row = $req->fetch()) {
        // Get articles from command id
        $reqArticles = DB->query('SELECT id_article, id_stock FROM invoice_article WHERE id_invoice = ' . $row['id']);
        $articles = [];
        while ($rowArticles = $reqArticles->fetch()) {
            // Get article name and price from article id
            $reqArticle = DB->query('SELECT name, price FROM article WHERE id = ' . $rowArticles['id_article']);
            $rowArticle = $reqArticle->fetch();

            // Get article size from stock id
            $reqStock = DB->query('SELECT id, size FROM stock WHERE id = ' . $rowArticles['id_stock']);
            $rowStock = $reqStock->fetch();


            $articles[] = [
                'id' => $rowArticles['id_article'],
                'name' => $rowArticle['name'],
                'price' => $rowArticle['price'],
                'stock' => $rowStock,
            ];
        }

        $commands[] = [
            'id' => $row['id'],
            'date' => $row['date'],
            'amount' => $row['amount'],
            'status' => $row['status'],
            'articles' => $articles,
        ];
    }

    return $commands;
}