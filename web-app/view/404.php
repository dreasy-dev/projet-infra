<!DOCTYPE html>
<html>

    <?php
    $title = "Erreur 404 - " . SITE_NAME;
    require_once('./view/template/head.php');
    ?>

    <body>
		<header>
            <?php require_once('./view/template/navbar.php'); ?>
            <section id="hero-page">
				<h1>Erreur 404</h1>
				<p><a href="/">Accueil</a> > Erreur 404</p>
            </section>
        </header>
        
		<main>
			<p class="no-one-found">Erreur 404 : La page demandée n'existe pas.<br/><a href="/">Retour à l'accueil</a></p>
		</main>

        <footer>
            <?php
            require_once('./view/template/footer.php');
            ?>
        </footer>
	</body>
</html>