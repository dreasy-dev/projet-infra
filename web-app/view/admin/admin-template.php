<?php
    if(!isset($_SESSION)){
        session_start();  
    }
?> 

<!DOCTYPE html>
<html>

    <?php   
    $title = "Administration - " . SITE_NAME;
    require_once('./view/template/head.php');
    ?>

    <head>
        <link href="/view/assets/css/admin-style.css" rel="stylesheet" />
    </head>

    <body>
        <div class="page-container">
            <div class="page-sidebar">
                <ul>
                    <li>
                        <a href="/">Retourner au site</a>
                    </li>
                    <li>
                        <a <?php if ($page == "home") : ?>class="active"<?php endif ?> href="/admin">Accueil</a>
                    </li>
                    <li>
                        <a <?php if (str_starts_with($page, "users")) : ?>class="active"<?php endif ?> href="/admin/users">Utilisateurs</a>
                        <ul class="sub-menu">
                            <li><a <?php if ($page == "users.home") : ?>class="active"<?php endif ?> href="/admin/users">Afficher</a></li>
                            <li><a <?php if ($page == "users.search") : ?>class="active"<?php endif ?> href="/admin/users/search">Rechercher</a></li>
                            <li><a <?php if ($page == "users.admins") : ?>class="active"<?php endif ?> href="/admin/users/admins">Administrateurs</a></li>
                        </ul>
                    </li>
                    <li>
                        <a <?php if (str_starts_with($page, "articles")) : ?>class="active"<?php endif ?> href="/admin/articles">Articles</a>
                        <ul class="sub-menu">
                            <li><a <?php if ($page == "articles.home") : ?>class="active"<?php endif ?> href="/admin/articles">Afficher</a></li>
                            <li><a <?php if ($page == "articles.add") : ?>class="active"<?php endif ?> href="/admin/articles/add">Ajouter un article</a></li>
                        </ul>
                    </li>
                    <li>
                        <a <?php if (str_starts_with($page, "filieres")) : ?>class="active"<?php endif ?> href="/admin/filieres">Filières</a>
                        <ul class="sub-menu">
                            <li><a <?php if ($page == "filieres.home") : ?>class="active"<?php endif ?> href="/admin/filieres">Afficher</a></li>
                            <li><a <?php if ($page == "filieres.add") : ?>class="active"<?php endif ?> href="/admin/filieres/add">Ajouter une filière</a></li>
                        </ul>
                    </li>
                    <li>
                        <a <?php if (str_starts_with($page, "ycoins")) : ?>class="active"<?php endif ?> href="/admin/ycoins">Ycoins</a>
                        <ul class="sub-menu">
                            <li><a <?php if ($page == "ycoins.methods") : ?>class="active"<?php endif ?> href="/admin/ycoins/methods">Moyens de paiement</a></li>
                            <li><a <?php if ($page == "ycoins.credit") : ?>class="active"<?php endif ?> href="/admin/ycoins/credit">Créditer</a></li>
                            <li><a <?php if ($page == "ycoins.debit") : ?>class="active"<?php endif ?> href="/admin/ycoins/debit">Débiter</a></li>
                        </ul>
                    </li>
                    <li>
                        <a <?php if (str_starts_with($page, "history")) : ?>class="active"<?php endif ?> href="/admin/history">Historique</a>
                        <ul class="sub-menu">
                            <li><a <?php if ($page == "history.credit") : ?>class="active"<?php endif ?> href="/admin/history/credits">Crédits</a></li>
                            <li><a <?php if ($page == "history.purchases") : ?>class="active"<?php endif ?> href="/admin/history/purchases">Achats</a></li>
                            <li><a <?php if ($page == "history.exchanges") : ?>class="active"<?php endif ?> href="/admin/history/exchanges">Échanges</a></li>
                        </ul>
                    </li>
                    <li>
                        <a <?php if ($page == "stats") : ?>class="active"<?php endif ?> href="/admin/stats">Statistiques</a>
                    </li>
                </ul>
            </div> 
			<div class="page-content">
                <?php 
                    switch ($page) {
                        case "home":
                            require_once('./view/admin/index.php');
                            break;

                        case "users.home":
                            require_once('./view/admin/users/index.php');
                            break;
                        case "users.search":
                            require_once('./view/admin/users/search.php');
                            break;
                        case "users.admins":
                            require_once('./view/admin/users/admins.php');
                            break;
                        case "users.one":
                            require_once('./view/admin/users/user.php');
                            break;
                        
                        case "articles.home":
                            require_once('./view/admin/articles/index.php');
                            break;
                        case "articles.add":
                            require_once('./view/admin/articles/add.php');
                            break;
                        case "articles.one":
                            require_once('./view/admin/articles/article.php');
                            break;

                        case "filieres.home":
                            require_once('./view/admin/filieres/index.php');
                            break;
                        case "filieres.add":
                            require_once('./view/admin/filieres/add.php');
                            break;
                        case "filieres.one":
                            require_once('./view/admin/filieres/filiere.php');
                            break;

                        default:
                            echo "<p>Erreur : Contenu introuvable</p>";
                            break;
                    }
                ?>
            </div>
        </div>
    </body>
</html>