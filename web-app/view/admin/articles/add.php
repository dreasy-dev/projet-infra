<h1>Ajouter un article</h1>
<p class="breadcrumbs"><a href="/">Yshop</a> > <a href="/admin">Administration</a> > <a href="/admin/articles">Articles</a> > Ajouter</p>

<form action="/admin/articles/add" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <label for="name">Nom de l'article</label>
        <input type="text" name="name" id="name" class="form-control" required>
    </div>
    <div class="form-group">
        <label for="description">Description de l'article</label>
        <textarea name="description" id="description" class="form-control" required></textarea>
    </div>
    <div class="form-group">
        <label for="price">Prix de l'article</label>
        <input type="number" name="price" id="price" class="form-control" required>
    </div>
    <div class="form-group">
        <label for="image">Image de l'article</label>
        <!--<input type="text" name="image" id="image" class="form-control" required>-->
        <input type="file" name="image" id="image" class="form-control" required>
    </div>
    <div class="form-group">
        <label for="filiere">Filière de l'article</label>
        <select name="filiere" id="filiere" class="form-control" required>
            <option value="0">Hors filière</option>
            <?php foreach($filieres as $filiere): ?>
                <option style="background-color: <?= $filiere['color'] ?>" value="<?= $filiere['id'] ?>"><?= $filiere['name'] ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Ajouter</button>
</form>
