<h1>Articles</h1>
<p class="breadcrumbs"><a href="/">Yshop</a> > <a href="/admin">Administration</a> > Articles</p>

<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Dénomination</th>
            <th>Filière</th>
            <th>Prix</th>
            <th>Stocks</th>
            <th>Visibilité</th>
            <th>Date de publication</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($articles as $article) : ?>
            <tr>
                <td><?= $article['id'] ?></td>
                <td><img src="<?= $article['picture'] ?>"> <?= $article['name'] ?></td>
                <td><?= $article['filiere'] ?></td>
                <td><?= $article['price'] ?></td>
                <td>
                    <div class="article-sizes">
                    <?php foreach($article['stocks'] as $stock): ?>
                        <?php if ($stock['number'] > 0): ?>
                            <p class="size in-stock"><?php print_r($stock['size']); ?></p>
                        <?php else: ?>
                            <p class="size out-stock"><?php print_r($stock['size']); ?></p>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </div>
                    <div class="article-sizes">
                    <?php foreach($article['stocks'] as $stock): ?>
                        <?php if ($stock['number'] > 0): ?>
                            <p class="size in-stock"><?php print_r($stock['number']); ?></p>
                        <?php else: ?>
                            <p class="size out-stock"><?php print_r($stock['number']); ?></p>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </div>
                </td>
                <td>Oui</td>
                <td><?= $article['publication'] ?></td>
                <td>
                    <a href="/admin/article?id=<?= $article['id'] ?>" class="table-btn table-btn-primary">Voir / Modifier</a> 
                    <form action="/admin/articles/delete" method="POST" style="display: inline-block;">
                        <input type="hidden" name="id" value="<?= $article['id'] ?>">
                        <button type="submit" class="table-btn table-btn-delete">Supprimer</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>