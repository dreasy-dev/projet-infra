<h1>Ajouter une filière</h1>
<p class="breadcrumbs"><a href="/">Yshop</a> > <a href="/admin">Administration</a> > <a href="/admin/filieres">Filières</a> > Ajouter</p>

<form action="/admin/filieres/add" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <label for="name">Nom de la filière</label>
        <input type="text" name="name" id="name" class="form-control" required>
    </div>
    <div class="form-group">
        <label for="color">Couleur de la filière</label>
        <input type="color" name="color" id="color" class="form-control" required>
    </div>
    <button type="submit" class="btn btn-primary">Ajouter</button>
</form>