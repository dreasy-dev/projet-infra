<h1>Filières</h1>
<p class="breadcrumbs"><a href="/">Yshop</a> > <a href="/admin">Administration</a> > Filières</p>

<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Dénomination</th>
            <th>Couleur</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($filieres as $filiere) : ?>
            <tr>
                <td><?= $filiere['id'] ?></td>
                <td><?= $filiere['name'] ?></td>
                <td><span style="padding: 5px; background-color: <?= $filiere['color'] ?>"></span> -> <?= $filiere['color'] ?></td>
                <td>
                    <a href="/admin/filiere?id=<?= $filiere['id'] ?>" class="table-btn table-btn-primary">Voir / Modifier</a> 
                    <form action="/admin/filieres/delete" method="POST" style="display: inline-block;">
                        <input type="hidden" name="id" value="<?= $filiere['id'] ?>">
                        <button type="submit" class="table-btn table-btn-delete">Supprimer</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>