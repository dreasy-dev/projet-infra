<h1>Utilisateurs</h1>
<p class="breadcrumbs"><a href="/">Yshop</a> > <a href="/admin">Administration</a> > Utilisateurs</p>

<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Prénom</th>
            <th>Nom</th>
            <th>E-mail</th>
            <th>Ycoins</th>
            <th>Rôle</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user) : ?>
            <tr>
                <td><?= $user['id'] ?></td>
                <td><?= $user['firstName'] ?></td>
                <td><?= $user['lastName'] ?></td>
                <td><?= $user['mail'] ?></td>
                <td><?= $user['credits'] ?></td>
                <td><?php if (empty($user['role'])) : echo "user"; else : echo $user['role']; endif; ?></td>
                <td>
                <a href="/admin/user?id=<?= $user['id'] ?>" class="table-btn table-btn-primary">Voir / Modifier</a> 
                <form action="/admin/users/admin" method="POST" style="display: inline-block;">
                    <input type="hidden" name="id" value="<?= $user['id'] ?>">
                    <button type="submit" class="table-btn table-btn-warning"><?php if (!userIsAdmin($user['id'])) : ?>Donner les droits admin<?php else : ?>Retirer les droits admin<?php endif ?></button>
                </form>
                <form action="/admin/users/delete" method="POST" style="display: inline-block;">
                    <input type="hidden" name="id" value="<?= $user['id'] ?>">
                    <button type="submit" class="table-btn table-btn-delete">Supprimer</button>
                </form>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>