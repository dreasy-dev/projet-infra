<!DOCTYPE html>
<html>

    <?php
    $title = $article['name'] . " - " . SITE_NAME;
    require_once('./view/template/head.php');
    ?>

    <body>
		<header>
            <?php require_once('./view/template/navbar.php'); ?>
            <section id="hero-page">
				<h1><?= $article['name']; ?></h1>
				<p><a href="/">Accueil</a> > <a href="/articles">Tous les articles</a> > <?= $article['name']; ?></p>
            </section>
        </header>
		<main>
        	<section class="article-container">
				<div class="article-col col-50">
					<img class="artice-picture" src="<?= $article['picture']; ?>" alt="<?= $article['name']; ?>">
				</div>
				<div class="article-col col-50">
					<h2><?= $article['name']; ?></h2>
					<p>Prix : <?= $article['price']; ?> ycoins</p>

					<form class="add-to-cart-form" action="/cart/add" method="POST">
						<input type="hidden" name="id" value="<?= $_GET['id'] ?>">
						<select name="stock" id="stock">
							<?php foreach($article['stocks'] as $stock): ?>
								<option value="<?= $stock['id']; ?>" <?php if($stock['number'] < 1) : ?>disabled<?php endif ?>><?= $stock['size']; ?></option>
							<?php endforeach; ?>
						</select>
						<input class="btn btn-primary" type="submit" value="Ajouter au panier">
					</form>
					<?php if (!empty($article['description'])): ?>
						<h3>Description :</h3>
						<p>
							<?= nl2br(htmlspecialchars($article['description'])); ?>
						</p>
					<?php endif; ?>
					<?php if (!empty($article['stocks'])): ?>
						<h3>Stocks encore disponibles :</h3>
						<ul>
							<?php foreach($article['stocks'] as $stock): ?>
								<li><?= $stock['size']; ?> : <?= $stock['number']; ?></li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				</div>
			</section>
		</main>

		<footer>
            <?php
            require_once('./view/template/footer.php');
            ?>
        </footer>
	</body>
</html>