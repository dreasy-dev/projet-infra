<!DOCTYPE html>
<html>

    <?php
    $title = "Tous nos articles en vente - " . SITE_NAME;
    require_once('./view/template/head.php');
    ?>

    <body>
        <header>
            <?php require_once('./view/template/navbar.php'); ?>
            <section id="hero-page">
                <?php if (isset($_GET['filiere'])): ?>
                    <h1><?= getFiliereName(htmlspecialchars($_GET['filiere'])); ?></h1>
                    <p><a href="/">Accueil</a> > <a href="/articles">Tous les articles</a> > <?= getFiliereName(htmlspecialchars($_GET['filiere'])); ?></p>
                <?php else: ?>
                    <h1>Tous nos articles</h1>
                    <p><a href="/">Accueil</a> > Tous les articles</p>
                <?php endif ?>
            </section>
        </header>

        <main>
            <section id="articles-home" class="home-section">
                
                <div class="section-content">
                    <div class="articles">
                        <?php if(count($articles) == 0) : ?>
                            <p class="no-one-found">Aucun article n'a été trouvé.<br/><a href="/">Retour à l'accueil</a></p>
                        <?php endif ?>
                        <?php foreach($articles as $article): ?>
                            <a class="article-card" href="article?id=<?= $article['id']; ?>">
                                <?php if (isset($article['picture'])): ?>
                                    <img src="<?= htmlspecialchars($article['picture']); ?>" alt="<?= htmlspecialchars($article['name']); ?>">
                                <?php else: ?>
                                    <img src="./view/assets/images/articles/default.png" alt="<?= htmlspecialchars($article['name']); ?>">
                                <?php endif ?>
                                <h3><?= htmlspecialchars($article['name']); ?></h3>
                                <?php if($article['filiere']) : ?>
                                    <p class="filiere"><?= htmlspecialchars($article['filiere']); ?></p>
                                <?php endif; ?>
                                <div class="article-sizes">
                                    <?php foreach($article['stocks'] as $stock): ?>
                                        <?php if ($stock['number'] > 0): ?>
                                            <p class="in-stock"><?php print_r($stock['size']); ?></p>
                                        <?php else: ?>
                                            <p class="out-stock"><?php print_r($stock['size']); ?></p>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </section>
        </main>

        <footer>

        </footer>
    </body>
</html>