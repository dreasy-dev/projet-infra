<!DOCTYPE html>
<html>

    <?php
    $title = "Mon panier - " . SITE_NAME;
    require_once('./view/template/head.php');
    ?>

    <body>
		<header>
            <?php require_once('./view/template/navbar.php'); ?>
            <section id="hero-page">
				<h1>Mon panier</h1>
				<p><a href="/">Accueil</a> > Mon panier</p>
            </section>
        </header>
		<main>
            <?php if (empty($cart)) : ?>
                <p class="no-one-found">Il y a vraiment beaucoup d'articles ici ! Votre panier est vide.<br/><a href="/articles">Continuer les achats</a></p>
            <?php else : ?>
                <?php $total = 0; ?>
                <div class="cart-container">
                    <div class="cart-content">
                        <h2>Mon panier</h2>
                        <?php foreach ($cart as $item) : ?>
                            <?php $article = getarticle($item['id_article']) ?>
                            <?php $total += $article['price'] ?>
                            <?php $stock = getStock($item['id_stock']) ?>

                            <a href="/article?id=<?= $item['id_article'] ?>" class="cart-article">
                                <img src="<?= $article['picture'] ?>" alt="<?= $article['name'] ?>">
                                <div>
                                    <h3><?= $article['name'] ?></h3>
                                    <p>Taille : <?= $stock['size'] ?></p>
                                    <p class="price">Prix : <?= $article['price'] ?> Ycoins</p>
                                </div>
                                <form action="/cart/remove" method="POST">
                                    <input type="hidden" name="id" value="<?= $item['id'] ?>">
                                    <input type="submit" value="Supprimer">
                                </form>
                            </a>
                        <?php endforeach ?>
                    </div>
                    <div class="cart-summary">
                        <h2>Récapitulatif</h2>
                        <p>Tous les paniers réservés sont à venir récupérer au pôle communication (à l'accueil du campus) sous 7 jours. Passé ce délai, les articles seront remis en vente.</p>

                        <div class="coins-zone">
                            <p>Vos Ycoins : <?= userGetCoins() ?></p>
                            <p><strong>Total du panier : <?= $total ?> Ycoins</strong></p>
                        </div>
                        <?php if (userGetCoins() < $total) : ?>
                            <p class="error-text">Vous n'avez pas assez de Ycoins pour valider votre panier.</p>
                            <a href="/profile/ycoins">Recharger votre compte</a>
                        <?php else : ?>
                            <form action="/cart/buy" method="POST">
                                <input type="submit" class="btn btn-primary" value="Passer la commande">
                            </form>
                            <p class="remaining-coins">Il vous restera <?= userGetCoins() - $total ?> Ycoins après cette commande.</p>
                        <?php endif ?>
                    </div>
                </div>
            <?php endif ?>
		</main>

        <footer>
            <?php
            require_once('./view/template/footer.php');
            ?>
        </footer>
	</body>
</html>