<!DOCTYPE html>
<html>

    <?php
    $title = "Mon panier - " . SITE_NAME;
    require_once('./view/template/head.php');
    ?>

    <body>
		<header>
            <?php require_once('./view/template/navbar.php'); ?>
            <section id="hero-page">
				<h1>Commande validée</h1>
				<p><a href="/">Accueil</a> > <a href="/cart">Mon panier</a> > Commande validée</p>
            </section>
        </header>
		<main>
            <?php $total = 0; ?>
            <div class="cart-container">
                <div class="cart-content">
                    <h2>Contenu de ma commande</h2>
                    <?php foreach ($cart as $item) : ?>
                        <?php $article = getarticle($item['id_article']) ?>
                        <?php $total += $article['price'] ?>
                        <?php $stock = getStock($item['id_stock']) ?>

                        <a href="/article?id=<?= $item['id_article'] ?>" class="cart-article">
                            <img src="<?= $article['picture'] ?>" alt="<?= $article['name'] ?>">
                            <div>
                                <h3><?= $article['name'] ?></h3>
                                <p>Taille : <?= $stock['size'] ?></p>
                                <p class="price">Prix : <?= $article['price'] ?> Ycoins</p>
                            </div>
                        </a>
                    <?php endforeach ?>
                </div>
                <div class="cart-summary">
                    <h2>Récapitulatif</h2>
                    <p>Tous les paniers réservés sont à venir récupérer au pôle communication (à l'accueil du campus) sous 7 jours. Passé ce délai, les articles seront remis en vente.</p>

                    <div class="coins-zone">
                        <p>Vos Ycoins : <?= userGetCoins() ?></p>
                        <p><strong>Total du panier : <?= $total ?> Ycoins</strong></p>
                    </div>
                </div>
            </div>
		</main>

        <footer>
            <?php
            require_once('./view/template/footer.php');
            ?>
        </footer>
	</body>
</html>