<!DOCTYPE html>
<html>
    
    <?php
    $title = SITE_NAME . " - La boutique en ligne de Ynov Campus";
    require_once('./view/template/head.php');
    ?>

    <body>
        <header>
            <?php require_once('./view/template/navbar.php'); ?>
            <section id="hero-home">
                <h1>Bienvenue sur YShop !</h1>
                <p>Découvrez dès maintenant la boutique de Ynov Campus !</p>
                <a class="btn btn-primary" href="/articles">Tous les articles</a>
            </section>
        </header>

        <main>
            <section id="articles-home" class="home-section">
                <h2 class="title title-black">Best-sellers</h2>
                <div class="section-content">
                    <div class="articles">
                        <?php foreach($articles as $article): ?>
                            <a class="article-card" href="article?id=<?= $article['id']; ?>">
                                <?php if (isset($article['picture'])): ?>
                                    <img src="<?= htmlspecialchars($article['picture']); ?>" alt="<?= htmlspecialchars($article['name']); ?>">
                                <?php else: ?>
                                    <img src="./view/assets/images/articles/default.png" alt="<?= htmlspecialchars($article['name']); ?>">
                                <?php endif ?>
                                <h3><?= htmlspecialchars($article['name']); ?></h3>
                                <div class="article-sizes">
                                <?php foreach($article['stocks'] as $stock): ?>
                                    <?php if ($stock['number'] > 0): ?>
                                        <p class="size in-stock"><?php print_r($stock['size']); ?></p>
                                    <?php else: ?>
                                        <p class="size out-stock"><?php print_r($stock['size']); ?></p>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                </div>
                            </a>
                        <?php endforeach; ?>
                    </div>
                    <div class="link-more">
                        <a class="arrow-link" href="/articles">Voir plus</a>
                    </div>
                </div>
            </section>
            <section id="filieres-home" class="home-section">
                <h2 class="title title-white">Chacun sa filière !</h2>
                <div class="section-content">
                    <div class="filiere-grid">
                        <?php foreach ($filieres as $filiere): ?>
                            <a href="/articles?filiere=<?= $filiere['id'] ?>" class="filiere-grid-item" style="background-color: <?= $filiere['color'] ?>">
                                <h3><?= $filiere['name'] ?></h3>
                            </a>
                        <?php endforeach; ?>
                    </div>
                    <div class="link-more">
                        <a class="arrow-link" href="/articles?filiere=0">Hors filières</a>
                    </div>
                </div>
            </section>
        </main>

        <footer>
            <?php
            require_once('./view/template/footer.php');
            ?>
        </footer>
    </body>
</html>