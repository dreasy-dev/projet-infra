<!DOCTYPE html>
<html>

    <?php
    $title = "Connexion - " . SITE_NAME;
    require_once('./view/template/head.php');
    ?>

    <body>
        <div class="login-body">
        </div>

        <div class="warning-message">
            <p>FACTICE - PROJET ÉTUDIANT</p>
            <p>Ce site est un projet étudant, <strong>n'y mettez pas vos vraies coordonnées YNOV</strong>.</p>
        </div>

        <div class="login-content">
            <div class="login-box">
            <div class="sidebar">
                <div class="sidebar-content">
                    <h3 class="desktop-only">Bienvenue sur Yshop</h3>
                    <p class="desktop-only">
                        Ce compte est différent de votre compte Ynov. Il vous permet d'utiliser vos Ycoins pour acheter de nouveaux goodies Ynov.
                    </p>
                </div>
                <div class="sidebar-footer">
                    <p class="desktop-only">
                        <a class="btn btn-white" href="/register">Créer un compte</a>
                    </p>
                </div>
            </div>
            <div class="main">

                <?php 
                    if(!empty($login_err)){
                        echo '<div class="alert alert-danger">' . $login_err . '</div>';
                    }        
                ?>
                <form action="/login" method="post">
                    <div class="main-title">
                        <h1>Connexion</h1>
                    </div>
                    <div class="form-group">
                        <label>Email :</label><br/>
                        <input type="text" name="email" class="form-control <?php echo (!empty($email_err)) ? 'is-invalid' : ''; ?>" value="">
                        <span class="invalid-feedback"><?php echo $email_err; ?></span>
                    </div>    
                    <div class="form-group">
                        <label>Mot de passe :</label><br/>
                        <input type="password" name="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>">
                        <span class="invalid-feedback"><?php echo $password_err; ?></span>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Connexion">
                    </div>
                    <p><a href="/">Retour à l'accueil</a></p>
                </form>
            </div>
        </div>
    </body>
</html>