<h2>Mes commandes</h2>
<p>Retrouvez ici vos dernières commandes : en cours, terminées ou en attente.</p>

<div class="commands-area ">
    <?php if (empty($commandsInProgress) && empty($commandsDelivered) && empty($commandsCanceled)) : ?>
        <p>Vous n'avez pas encore passé de commande.</p>
    <?php endif; ?>
    
    <?php if (!empty($commandsInProgress)) : ?>
        <h3>Commandes à récupérer</h3>
        <?php foreach ($commandsInProgress as $command): ?>
            <?php if ($command['status'] == 'En attente'): ?>
                <div class="command-overview">
                    <h4>Commande n°<?= $command['id'] ?></h4>
                    <p>Commande passée le <?= $command['date'] ?></p>
                    <p>Statut : <?= $command['status'] ?></p>
                    <p>Montant : <?= $command['amount'] ?> Ycoins</p>
                    <p>Articles commandés :</p>
                    <ul>
                        <?php foreach ($command['articles'] as $article): ?>
                            <li><?= $article['name'] ?> (<?= $article['stock']['size'] ?>)</li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php if (!empty($commandsDelivered)) : ?>
        <h3>Commandes récupérées</h3>
        <?php foreach ($commandsDelivered as $command): ?>
            <?php if ($command['status'] == 'Terminée'): ?>
                <div>
                    <h4>Commande n°<?= $command['id'] ?></h4>
                    <p>Commande passée le <?= $command['date'] ?></p>
                    <p>Statut : <?= $command['status'] ?></p>
                    <p>Montant : <?= $command['amount'] ?> €</p>
                    <p>Articles commandés :</p>
                    <ul>
                        <?php foreach ($command['articles'] as $article): ?>
                            <li><?= $article['name'] ?> (<?= $article['size'] ?>) x<?= $article['quantity'] ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php if (!empty($commandsCanceled)) : ?>
        <h3>Commandes annulées</h3>
        <?php foreach ($commandsCanceled as $command): ?>
            <?php if ($command['status'] == 'Annulée'): ?>
                <div>
                    <h4>Commande n°<?= $command['id'] ?></h4>
                    <p>Commande passée le <?= $command['date'] ?></p>
                    <p>Statut : <?= $command['status'] ?></p>
                    <p>Montant : <?= $command['amount'] ?> €</p>
                    <p>Articles commandés :</p>
                    <ul>
                        <?php foreach ($command['articles'] as $article): ?>
                            <li><?= $article['name'] ?> (<?= $article['size'] ?>) x<?= $article['quantity'] ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
