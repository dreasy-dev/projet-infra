<h2>Bienvenue, <?= userGetFirstName() ?> !</h2>
<p>Votre compte Yshop en un clin d'oeil : consultez et gérez vos commandes.<br/>
Rechargez également votre compte en Ycoins, ou envoyez-en à vos amis !</p>

<div class="profile-cta">
    <a class="profile-cta-infos" href="/profile/infos">
        <h3>Mes informations</h3>
        <p>Consultez/modifiez vos informations.</p>
    </a>
    <a class="profile-cta-commands" href="/profile/commands">
        <h3>Mes commandes</h3>
        <p>Consultez l'historique de vos commandes.</p>
    </a>
    <a class="profile-cta-ycoins" href="/profile/ycoins">
        <h3>Mes Ycoins</h3>
        <p>Rechargez votre compte en Ycoins.</p>    
    </a>
</div>

        