<h2>Mes informations</h2>
<p>Retrouvez sur cette page vos informations personnelles.<br />Pour modifier vos informations ou votre mot de passe, cliquez sur "Modifier mes informations".</p>


<div class="profile-infos">
    <div class="profile-zone-pic">
        <img src="<?= userGetPicture() ?>" alt="Photo de profil" class="profile-picture">
    </div>

    <div class="profile-zone-infos">
        <form action="/profile/infos/modify" method="POST">
        <h3>Nom</h3>
            <input name="lastName" value="<?= userGetLastName() ?>" <?php if($page == "infos") : ?>disabled<?php endif ?>></input>
        <h3>Prénom</h3>
            <input name="firstName" value="<?= userGetFirstName() ?>" <?php if($page == "infos") : ?>disabled<?php endif ?>></input>
        <h3>Adresse email</h3>
            <input name="email" value="<?= userGetEmail() ?>" <?php if($page == "infos") : ?>disabled<?php endif ?>></input>
        <?php if($page == "infos.modify") : ?>
            <h3>Nouveau mot de passe</h3>
            <input name="password" type="password" placeholder="Mot de passe"></input>
            <input name="passwordConfirm" type="password" placeholder="Confirmation"></input>
        <?php endif ?>
    </div>
</div>

<?php if($page == "infos") : ?><a class="btn btn-primary" href="/profile/infos/modify">Modifier mes informations</a><?php endif ?>
<?php if($page == "infos.modify") : ?><button type="submit" class="btn btn-primary">Enregistrer</button> <a class="btn btn-black" href="/profile/infos">Annuler</a><?php endif ?>