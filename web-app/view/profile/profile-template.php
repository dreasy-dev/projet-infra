<?php
    if(!isset($_SESSION)){
        session_start();  
    }
?> 

<!DOCTYPE html>
<html>

    <?php   
    $title = "Mon profil - " . SITE_NAME;
    require_once('./view/template/head.php');
    ?>

    <body>
        <header>
            <?php require_once('./view/template/navbar.php'); ?>
            <section id="hero-page">
                <h1>Mon compte</h1>
                <p><a href="/">Accueil</a> > Mon compte</p>
            </section>
        </header>

        <main>
            <div class="profile-row">
                <div class="profile-col col-25">
                    <div class="container">
                        <ul class="menu-list">
                            <li>
                                <a <?php if ($page == "home") : ?>class="active"<?php endif ?> href="/profile">
                                    <div>
                                        <span>Accueil</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a <?php if (str_starts_with($page, "infos")) : ?>class="active"<?php endif ?> href="/profile/infos">
                                    <div>
                                        <span>Mes informations</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($page == "commands") : ?>class="active"<?php endif ?> href="/profile/commands" target="">
                                    <div>
                                        <span>Mes commandes</span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <ul class="menu-list middle">
                            <li class="Item-pfhot3-2 kRHUJs">
                                <a <?php if ($page == "ycoins") : ?>class="active"<?php endif ?> href="/profile/ycoins" target="">
                                    <div>
                                        <span>Recharger mes Ycoins</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($page == "ycoins-send") : ?>class="active"<?php endif ?> href="/profile/ycoins/send" target="">
                                    <div>
                                        <span>Envoyer des Ycoins</span>
                                    </div>
                                </a>
                            </liclass=>                                      
                        </ul>
                        <ul class="menu-list">
                            <?php if(userIsAdmin()): ?>
                                <li>
                                    <a href="/admin">
                                        <div>
                                            <span>Accès administrateur</span>
                                        </div>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <li>
                                <a href="/logout">
                                    <div>
                                        <span>Déconnexion</span>
                                    </div>
                                </a>
                            </li>                                      
                        </ul>
                    </div>
                </div>
                <div class="profile-col col-75">
                    <?php
                    if($page == "home") : 
                        require('view/profile/index.php');
                    elseif (str_starts_with($page, "infos")) :
                        require('view/profile/infos.php');
                    elseif ($page == "commands") :
                        require('view/profile/commands.php');
                    elseif ($page == "ycoins") :
                        require('view/profile/ycoins/index.php');
                    elseif ($page == "ycoins-send") :
                        require('view/profile/ycoins/send.php');
                    endif;
                    ?>
                </div>
            </div>
        </main>

        <footer>
            <?php
            require_once('./view/template/footer.php');
            ?>
        </footer>
    </body>
</html>