<h2>Mes Ycoins</h2>
<p>Gérez ici vos Ycoins et rechargez votre compte dès maintenant !</p>

<p>Vous avez actuellement <strong><?= userGetCoins() ?></strong> Ycoins.</p>

<h2>Recharger mon compte</h2>

<h3>Via Paypal</h3>
<div class="payment-options">
    <div class="ycoins-card">
        <h3>1 000 Ycoins</h3>
        <p>1.00 €</p>
        <form action="/profile/ycoins" method="post">
            <input type="hidden" name="amount" value="1000">
            <button type="submit" class="btn btn-primary">Acheter</button>
        </form>
    </div>
    <div class="ycoins-card">
        <h3>3 000 Ycoins</h3>
        <p>2.50 €</p>
        <form action="/profile/ycoins" method="post">
            <input type="hidden" name="amount" value="1000">
            <button type="submit" class="btn btn-primary">Acheter</button>
        </form>
    </div>
    <div class="ycoins-card">
        <h3>6 000 Ycoins</h3>
        <p>5.00 €</p>
        <form action="/profile/ycoins" method="post">
            <input type="hidden" name="amount" value="1000">
            <button type="submit" class="btn btn-primary">Acheter</button>
        </form>
    </div>
    <div class="ycoins-card">
        <h3>10 000 Ycoins</h3>
        <p>8.50 €</p>
        <form action="/profile/ycoins" method="post">
            <input type="hidden" name="amount" value="1000">
            <button type="submit" class="btn btn-primary">Acheter</button>
        </form>
    </div>
    <div class="ycoins-card">
        <h3>15 000 Ycoins</h3>
        <p>13.00 €</p>
        <form action="/profile/ycoins" method="post">
            <input type="hidden" name="amount" value="1000">
            <button type="submit" class="btn btn-primary">Acheter</button>
        </form>
    </div>
    <div class="ycoins-card">
        <h3>20 000 Ycoins</h3>
        <p>15.00 €</p>
        <form action="/profile/ycoins" method="post">
            <input type="hidden" name="amount" value="1000">
            <button type="submit" class="btn btn-primary">Acheter</button>
        </form>
    </div>
</div>

<h3>Via Allopass</h3>
<div class="payment-options">
    <div class="ycoins-card">
        <h3>1 000 Ycoins</h3>
        <p>2.00 €</p>
        <form action="/profile/ycoins" method="post">
            <input type="hidden" name="amount" value="1000">
            <button type="submit" class="btn btn-primary">Acheter</button>
        </form>
    </div>
    <div class="ycoins-card">
        <h3>3 000 Ycoins</h3>
        <p>4.00 €</p>
        <form action="/profile/ycoins" method="post">
            <input type="hidden" name="amount" value="1000">
            <button type="submit" class="btn btn-primary">Acheter</button>
        </form>
    </div>
    <div class="ycoins-card">
        <h3>5 000 Ycoins</h3>
        <p>5.00 €</p>
        <form action="/profile/ycoins" method="post">
            <input type="hidden" name="amount" value="1000">
            <button type="submit" class="btn btn-primary">Acheter</button>
        </form>
    </div>
</div>