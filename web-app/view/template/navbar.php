            <div class="nav no-select">
                <a href="/" class="nav-logo">
                    <img src="https://www.ynov.com/wp-content/themes/ynov/dist/images/logo.svg" alt="Logo Yshop">
                </a>

                <div class="nav-sticky">
                    <a href="<?php if (userIsConnected()): ?>/profile<?php else: ?>/login<?php endif ?>" class="nav-item nav-account">
                        <img src="<?= userGetPicture(); ?>" alt="Photo de profil - <?= userGetName(); ?>">
                        <p>
                            <?php if (userIsConnected()): ?>
                                <?= userGetName(); ?>
                            <?php else: ?>
                                Me connecter
                            <?php endif; ?>
                        </p>
                    </a>
                    <a href="/cart" class="nav-item nav-cart">
                        <img src="/view/assets/images/cart.png" alt="Panier">
                    </a>
                </div>
            </div>